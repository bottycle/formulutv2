<?php

class Tools 
{
    private $ci; 
    public $mail_sender;


    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->load->library('email');
        
        $this->mail_sender = "contact@formul-ut.fr";

    }

    public function name($email)
    {
        $pattern = '/[0-9]/';
        $replacement = '';
        $string = preg_replace($pattern, $replacement, $email);

        $string = explode('.', $string);
        $prenom = $string[0];
        $tmp = explode('@', $string[1]);
        $nom = $tmp[0];

        return ucfirst($prenom) . " " . ucfirst($nom);

    }

    public function dateFR($date)
    {
        // Si il y a les heures
        if(strlen($date) > 10){
            return date('d/m/Y H:i:s', strtotime($date));
        }
        else{
            return date('d/m/Y', strtotime($date));
        }
    }

    public function slugify($string, $delimiter = '-')
    {
        $oldLocale = setlocale(LC_ALL, '0');
        setlocale(LC_ALL, 'en_US.UTF-8');
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower($clean);
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        $clean = trim($clean, $delimiter);
        setlocale(LC_ALL, $oldLocale);
        return $clean;
    }

    public function transform_message($message, $sujet="SESAME+") {
        $template_email = file_get_contents(__DIR__.'/MailTemplateNew.html');
        $templated_message = str_replace('--MESSAGE--', $message, $template_email);
        $templated_message = str_replace('--SUJET--', $sujet, $templated_message);
        return $templated_message;
    }

    /** 
     * Permet d'envoyer des mails facilement depuis tous les controllers 
     * @author Baptiste LEFEUVRE + Clément BOTTY
     * */
    public function sendMail($mail, $message, $sujet) {
        $this->ci->email->from($this->mail_sender, "Formul'UT");
        $this->ci->email->to($mail);
        $this->ci->email->subject($sujet);
        $this->ci->email->set_mailtype("html");
        $this->ci->email->message($this->ci->tools->transform_message($message, $sujet));

        $this->ci->email->send();
        # echo ($this->ci->email->send()) ? "Mail Sent to ".$mail : "Erreur ";
    }
}