<?php
	class Users extends CI_Controller{
		
		/**
		 * Inscription d'un nouvel utilisateur
		 */
		public function register(){

			$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists');
			$this->form_validation->set_rules('password', 'Mot de passe', 'required');
			$this->form_validation->set_rules('password2', 'Confirmation du mot de passe', 'matches[password]');

			if($this->form_validation->run() === FALSE){
				$this->load->view('templates/header');
				$this->load->view('users/register');
				$this->load->view('templates/footer');
			} else {
				// Encrypt password
				$enc_password = password_hash($this->input->post('password', TRUE), PASSWORD_DEFAULT);

				$this->user_model->register($enc_password);

				// Set message
				$this->session->set_flashdata('user_registered', "Votre inscription est en attente, vous recevez un mail lorsqu'elle sera validée");

				redirect('');
			}
		}

		/**
		 * Gestion de la connexion de l'utilisateur
		 * @author Clément
		 */
		public function login(){


			$this->form_validation->set_rules('email', 'Adresse E-Mail', 'required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required');
			
			if($this->form_validation->run() === FALSE){
				$this->load->view('templates/header');
				$this->load->view('users/login');
				$this->load->view('templates/footer');
			} else {
				
				// On récupère l'adresse E-Mail UTC
				$email = $this->input->post('email', TRUE);
				// Get and encrypt the password
				$password = $this->input->post('password', TRUE);

				// Login user
				$connect = $this->user_model->login($email);

				// Si l'utilisateur existe, que sont mdp est correcte, et qu'il est vérifié alors on le connecte
				if(!empty($connect) && password_verify($password, $connect['password']) && $connect['verified']){
					// On crée une session PHP pour stocker les identifiants
					$_SESSION['user_id'] = $connect['user_id'];
					$_SESSION['email'] = $email;
					$_SESSION['logged_in'] = true;
					
					// Set message
					$message = 'Bonjour '.$this->tools->name($email).' vous êtes désormais connecté.';
					$this->session->set_flashdata('user_loggedin', $message);

					redirect('');
				} // Si l'utilisateur n'est pas encore vérifié on lui demande d'être patient
				else if(!empty($connect) && password_verify($password, $connect['password']) && !$connect['verified']){
					$this->session->set_flashdata('not_verified', "Désolé, votre compte n'a pas encore été vérifié par un administrateur.");
					redirect('users/login');
				}// Si son email ou son mdp est incorrecte on lui affiche un message d'erreur
				else{
					$this->session->set_flashdata('login_failed', "Votre identifiant ou votre mot de passe est incorrecte.");
					redirect('users/login');
				}
			}
		}

		/**
		 * Gestion de la déconnexion
		 * @author Clément
		 */
		public function logout(){
			// Unset user data
			$this->session->unset_userdata('logged_in');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('email');

			// Set message
			$this->session->set_flashdata('user_loggedout', 'Vous êtes désormais déconnecté');

			// On redirige vers la page d'accueil
			redirect('');
		}

		/**
		 * Gestion de la vérification des utilisateurs
		 * @author Clément
		 * @param int ID de l'utilisateur
		 * Si un ID est fourni alors on passe verified à TRUE dans la base
		 * Sinon c'est juste qu'on veut afficher la liste des utilisateurs non vérifiés
		 */
		public function verify($user_id = FALSE){
			// Si on ne passe pas d'ID dans l'url
			if(!$user_id){
				// On récupère tous les utilisateurs non vérifiés ($verified = FALSE)
				$data =  array(
					"users" => $this->user_model->getUsers(FALSE)
				);

				$this->load->view('templates/header');
				$this->load->view('users/verify', $data);
				$this->load->view('templates/footer');
			}
			// Si on passe un nom d'utilisateur et que l'on est connecté (sécurité contre attaque GET)
			else if($user_id && $_SESSION['logged_in']){
				
				$this->user_model->verify($user_id);

				// Envoie d'une alerte
				$this->session->set_flashdata('user_verified', 'Utilisateur #'.$user_id.' vérifié avec succès.');
				redirect('users/verify');
			}

		}

		/**
		 * Permet de savoir si une adresse email à déjà été utilisé comme identifiant
		 */
		public function check_email_exists($email){
			$this->form_validation->set_message('check_username_exists', 'Un utilisateur avec cette adresse email existe déjà.');
			if($this->user_model->check_email_exists($email)){
				return true;
			} else {
				return false;
			}
		}

	}