<?php
	class User_model extends CI_Model{

		/**
		 * Permets d'inscrire un utilisateur
		 * Celui-ci est mis en attente de validation
		 * @author Clément
		 * @param string Mot de passe encrypté
		 */
		public function register($enc_password){
			// Données de l'utilisateur
			$data = array(
				'email' => $this->input->post('email'),
				'password' => $enc_password,
				'verified' => FALSE
			);

			// Création d'un nouvel utilisateur pas encore validé
			return $this->db->insert('users', $data);
		}

		/**
		 * Gestion de la connexion
		 * @author Clément
		 * @param string Adresse Email UTC de l'utilisateur
		 * @param string Mot de passe 
		 * @return int ID de l'utilisateur si mot de passe correct, FALSE sinon
		 */
		public function login($email){
			return $this->db->get_where('users', array('email' => $email))->row_array();
		}

		/**
		 * Vérifie si l'email de l'utilisateur existe
		 * @author Clément
		 * @param string Adresse email demandé par l'utilisateur
		 * @return boolean TRUE si l'email existe déjà, FALSE sinon
		 */
		public function check_email_exists($email){
			$query = $this->db->get_where('users', array('email' => $email));
			if(empty($query->row_array())){
				return TRUE;
			} else {
				return FALSE;
			}
		}

		public function getUsers($verified = TRUE){
			return $this->db->get_where('users', array('verified' => $verified))->result_array();
		}

		public function verify($user_id){
			$this->db->where('id', $user_id);
			$this->db->update('users', array('verified' => TRUE));
			return TRUE;
		}
	}