<?php echo form_open('users/login'); ?>
	<div class="row justify-content-center mt-3">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h1 class="text-center">Connexion</h1>
				</div>
				<div class="card-body">
					<div class="form-group">
						<input type="email" name="email" class="form-control" placeholder="Email UTC" required autofocus>
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Entrez votre mot de passe" required autofocus>
					</div>
					<button type="submit" class="btn btn-dark btn-block">SE CONNECTER</button>
				</div>
				
			</div>
		</div>
	</div>
<?php echo form_close(); ?>