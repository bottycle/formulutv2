<div class="row justify-content-center mt-3">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h1 class="text-center h3">Vérification des utilisateurs</h1>
            </div>
            <div class="card-body p-0">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nom / Prénom</th>
                        <th scope="col">Email UTC</th>
                        <th scope="col">Vérifier</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($users as $user): ?>
                        <tr>
                            <td><?php echo $user['id'] ?></td>
                            <td><?php echo $user['id'] ?></td>
                            <td><?php echo $user['email'] ?></td>
                            <td><a href="<?= base_url('users/verify/').$user['id'] ?>" class="btn btn-success mr-2">Valider</a><a href="<?= base_url('users/delete/').$user['id'] ?>" class="btn btn-danger">Supprimer</a></td>
                        </tr>
                        <?php endforeach ?>
                        
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>