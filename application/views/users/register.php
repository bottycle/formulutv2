<div class="alert alert-warning my-3">
	<p><?php echo validation_errors(); ?></p>
</div>
<?php echo form_open('users/register'); ?>
	<div class="row justify-content-center my-5">
		<div class="col-8">
			<div class="card">
				<div class="card-header">
					<h1 class="text-center">S'inscrire</h1>
				</div>
				<div class="card-body">
					<div class="form-group">
						<label>Adresse Email universitaire</label>
						<input type="email" class="form-control" name="email" placeholder="Ex : jean.dupont@etu.utc.fr">
					</div>
					<div class="form-group">
						<label>Mot de passe</label>
						<input type="password" class="form-control" name="password" placeholder="Mot de passe">
					</div>
					<div class="form-group">
						<label>Confirmez votre mot de passe</label>
						<input type="password" class="form-control" name="password2" placeholder="Entrez une deuxième fois votre mot de passe">
					</div>
					<button type="submit" class="btn btn-dark btn-block">S'INSCRIRE</button>
				</div>
				<!-- ./card-body -->
			</div>
			<!-- ./card  -->			
		</div>
		<!-- ./col -->
	</div>
	<!-- ./row -->
<?php echo form_close(); ?>
