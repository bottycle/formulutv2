		
	</div>
	<footer class="mt-4">
		<div class="footer-logos">
			<a onclick="window.open(this.href); return false;" href="https://www.facebook.com/formul.ut/"><i class="fab fa-facebook-f"></i></a>
			<a onclick="window.open(this.href); return false;" href="https://www.instagram.com/formul.ut/?hl=fr"><i class="fab fa-instagram"></i></a>
			<a onclick="window.open(this.href); return false;" href="mailto:contact@formul-ut.fr"><i class="fas fa-envelope"></i></a>
			<a onclick="window.open(this.href); return false;" href="https://www.linkedin.com/company/30606062/"><i class="fab fa-linkedin-in"></i></a>
		</div>
		<div class="footer-links">
			<span class="footer"><a class="footer-link" href="contact.php">Contact</a> | <a class="footer-link" href="mentions-legales.php">Mentions légales</a></span>
			<br/>
			<span class="footer">Copyright &copy; 2019 Formul&#039;UT</span>
		</div>
	</footer>

	<script>
		CKEDITOR.replace( 'editor1' );
	</script>
</body>
</html>


