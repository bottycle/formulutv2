<html>
	<head>
    <meta charset="utf-8">
    <meta charset="utf-8"><meta name="description" content="Formul'UT, Un des plus beaux projets de l'UTC : construire une monoplace électrique et participer à la Formula SAE !">
    <title>Formul&#039;UT</title>
		<link rel="stylesheet" href="<?= base_url('vendor/bootstrap/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">

    <link rel="shortcut icon" href="<?= base_url("assets/img/logo/favicon.ico") ?>" type="image/x-icon">
    <link rel="icon" href="<?= base_url("assets/img/logo/favicon.ico") ?>" type="image/x-icon">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/f92b2a05e8.js" crossorigin="anonymous"></script>
    <script src="http://cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
	</head>
	<body>



    <nav class="navbar navbar-expand-md navbar-dark bg-dark">

      <a class="navbar-brand" href="<?= base_url() ?>">
        <img src="<?= base_url('assets/img/logo/logo-blanc.png') ?>" width="200">
      </a>


      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="<?= base_url() ?>">ACCUEIL <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('posts/') ?>">ACTUALITÉS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">L'EQUIPE</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">VOITURE</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">CONTACT</a>
          </li>
        </ul>

        <!-- Menu utilisateur -->
        <?php if(isset($_SESSION['logged_in'])):?> <!-- On affiche seulement si l'utilisateur est connecté -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown">
            
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user-circle mr-1"></i><?php echo $this->tools->name($_SESSION['email'])?></a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="<?= base_url('posts/create/') ?>">Rédiger un article</a>
              <a class="dropdown-item" href="<?= base_url('users/verify/') ?>">Vérifier un utilisateur</a>
              <a class="dropdown-item" href="#">Changer le mot de passe</a>
              <a class="dropdown-item" href="<?= base_url('users/logout/') ?>">Déconnexion</a>
            </div>
          </li>
        </ul>
      <?php endif ?>

      </div>
    </nav>

    <div class="container mt-3">
      <!-- Flash messages -->
      <?php if($this->session->flashdata('user_registered')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_registered').'</p>'; ?>
      <?php endif; ?>

      <?php if($this->session->flashdata('post_created')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_created').'</p>'; ?>
      <?php endif; ?>

      <?php if($this->session->flashdata('post_updated')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_updated').'</p>'; ?>
      <?php endif; ?>

      <?php if($this->session->flashdata('category_created')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('category_created').'</p>'; ?>
      <?php endif; ?>

      <?php if($this->session->flashdata('post_deleted')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_deleted').'</p>'; ?>
      <?php endif; ?>

      <?php if($this->session->flashdata('login_failed')): ?>
        <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
      <?php endif; ?>

      <?php if($this->session->flashdata('user_loggedin')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>'; ?>
      <?php endif; ?>

       <?php if($this->session->flashdata('user_loggedout')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedout').'</p>'; ?>
      <?php endif; ?>

      <?php if($this->session->flashdata('category_deleted')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('category_deleted').'</p>'; ?>
      <?php endif; ?>