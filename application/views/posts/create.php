<h1 class="h2">Rédiger un article</h1>

<div class="alert alert-warning">
  <?php echo validation_errors(); ?>
</div>

<?php echo form_open_multipart('posts/create'); ?>
  <div class="form-group">
    <label>Titre de l'article</label>
    <input type="text" class="form-control" name="title" placeholder="Ajouter un article">
  </div>
  <div class="form-group">
    <label>Contenu</label>
    <textarea id="editor1" class="form-control" name="body" placeholder="Ajouter du contenu"></textarea>
  </div>
  <div class="form-group">
	  <label>Catégorie</label>
	  <select name="category_id" class="form-control">
		  <?php foreach($categories as $category): ?>
		  	<option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
		  <?php endforeach; ?>
	  </select>
  </div>
  <div class="form-group">
	  <label>Upload Image</label>
	  <input type="file" name="userfile" size="20">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>